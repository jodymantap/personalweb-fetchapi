import React, { Component } from "react";
import axios from 'axios'
import './style.css';
 
class HomeAxios extends Component {
   constructor(props){
       super(props)
       this.state = {
        items: []
       }
   }

   componentDidMount(){
       axios.get("https://api.github.com/users/jodymantap/repos")
    //    fetch("https://api.github.com/users/jodymantap/repos")
        .then(res => {
            const items = res.data
            this.setState({items});
            //console.log(items);
        })
        //.then(res => console.log(res))
        // .then(parsedJSON => parsedJSON.map(data => (
        //     {
        //         id: `${data.owner.id}`,
        //         firstName: `${data.owner.id}`,
        //         lastName: `${data.owner.login}`,
        //         location : `${data.owner.html_url}`,
        //         thumbnail: `${data.owner.avatar_url}`,
        //     }
        // )))
        // .then(items => this.setState({
        //     items
        // }))
        .catch(error => console.log('parsing data is failed', error))
   }

   render(){
       const{items} = this.state
       return(
        <div className="boxWhite">
               <h2>Random User</h2>
               {
                   items.length > 0 ? items.map(item => {
                       return(
                           <div key={item.owner.id} className="bgCircle">
                               <center><img style={{width: "120px"}} src={item.owner.avatar_url} alt={item.owner.id} className="circle"></img></center>
                               <div className="info">
                                   {item.owner.id} {item.owner.login} <br/>
                                   {item.owner.html_url}
                               </div>
                           </div>
                       )
                   }) 
                :null}
           </div>
       )
   }
}

export default HomeAxios;