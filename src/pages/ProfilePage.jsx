import React from 'react'
//import HomePage from './pages/HomePage'

const Profile = () => {
  return (
    <div className="App">
      {/* Contents */}
      <br/>
      <h1 className="text-center text-dark">Here's more about me...</h1>
      <hr/>
        <div className="container">
          <div className="row">
            <div className="col-md">
              <div className="card text-white bg-dark">
                <div className="card-body">
                  <h4 className="card-title text-center">About Me</h4>
                </div>
                <img src="https://images.pexels.com/photos/169573/pexels-photo-169573.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940" className="card-img-top" alt="..." />
                <div className="card-body">
                  <h4 className="card-subtitle text-center">Muhammad Adnand Jody Pratama</h4>
                  <h6 className="text-muted text-center">1999 - The end of the day</h6>
                  <p className="card-text text-center">I am a Front-end web developer Student at Glints Academy Batch 9. Currently learning react to make something awesome.</p>
                </div>
              </div>
            </div>
            <div className="col-md">
              <div className="card text-white bg-dark">
                <div className="card-body">
                  <h4 className="card-title text-center">Education</h4>
                </div>
                <img src="https://images.pexels.com/photos/163125/board-printed-circuit-board-computer-electronics-163125.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940" className="card-img-top" alt="..." />
                <div className="card-body">
                  <h4 className="card-subtitle text-center">Computer Engineering Politeknik Negeri Sriwijaya</h4>
                  <h6 className="text-muted text-center">2017 - 2020</h6>
                  <p className="card-text text-center">Here I got my first sight in Web Programming. We also learnt about microcontroller and stuffs.</p>
                </div>
              </div>
            </div>
            <div className="col-md">
              <div className="card text-white bg-dark">
                <div className="card-body">
                  <h4 className="card-title text-center">Experience</h4>
                </div>
                <img src="https://images.pexels.com/photos/6231/marketing-color-colors-wheel.jpg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940" className="card-img-top" alt="..." />
                <div className="card-body">
                  <h4 className="card-subtitle text-center">Student Ambassador cicil.co.id</h4>
                  <h6 className="text-muted text-center">February 2020 - August 2020</h6>
                  <p className="card-text text-center">Here I learnt about what I couldn't find on my college. Marketing, People Management and etc.</p>
                </div>
              </div>
            </div>
          </div>
        </div>
        <hr/>
      </div>
  );
}

export default Profile;

