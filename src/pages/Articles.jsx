import React from 'react'
//import HomePage from './pages/HomePage'

const Articles = () => {
  return (
    <div className="App">
        <br/>
        <h1 className="text-dark text-center">Here are some articles you might like : </h1>
        <br/>
        <hr/>
        <div className="container">
        {/* Cards */}
            <div className="row">
              <div className="col-md">
                <div className="card text-white bg-dark">
                  <img src="https://drive.google.com/uc?export=view&id=10oWR_ygrxcFBMjVkbUj61GIbS27NRkhe" className="card-img-top" alt="..."/>
                  <div className="card-body">
                    <h5 className="card-title">CICIL X Dompet Dhuafa</h5>
                    <p className="card-text">CICIL bersama @dompetdhuafaorg menginisiasi Gerakan #TetapBisaBelajar untuk memastikan para pelajar dan mahasiswa bisa mendapatkan...</p>
                    <a target="_blank" href="https://www.instagram.com/p/CB2AgiqAZsz/" className="btn btn-secondary">Details...</a>
                  </div>
                </div>
              </div>
              <div className="col-md">
                <div className="card text-white bg-dark">
                  <img src="https://drive.google.com/uc?export=view&id=1iGkCax0DwCBMhmT7GQHABmDWmEvWprjk" className="card-img-top" alt="..."/>
                  <div className="card-body">
                    <h5 className="card-title">Kelas CICIL Palembang</h5>
                    <p className="card-text">Yuk, jadi mahasiswa yang aktif di dalam dan di luar kampus biar ngga insecure terus! Di kelas cicil kali ini Kak Lestari dan Kak Jody akan membagikan pengalaman... </p>
                    <a target="_blank" href="https://www.instagram.com/p/CGtRIAesT63/" class="btn btn-secondary">Details...</a>
                  </div>
                </div>
              </div>
              <div className="col-md">
                <div className="card text-white bg-dark">
                  <img src="https://drive.google.com/uc?export=view&id=1i2nnBO6AJ-xMjB15SlqBA6J26968ZSv5" className="card-img-top" alt="..."/>
                  <div className="card-body">
                    <h5 className="card-title">CICIL Bilik Cerita</h5>
                    <p className="card-text">Tahun ini ngga dapet THR gara gara Corona? YUK IKUTAN CICIL BILIK CERITA CHALLENGE!
                      Caranya gampang...</p>
                    <a target="_blank" href="https://www.instagram.com/p/CBFrQrCnGKP/" class="btn btn-secondary">Details...</a>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <hr/>
        </div>
  );
}

export default Articles;

