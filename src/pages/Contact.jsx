import React from 'react'
//import HomePage from './pages/HomePage'

const Contact = () => {
  return (
    <div className="App">
        <br/>
        <h1 className="text-center text-dark">Any feedback?</h1>
        <hr/>
        <div className="container">
            <h3 class="text-center text-muted">Contact me</h3>
            <form>
                <div className="form-group">
                    <label for="exampleInputEmail1">Your Email address</label>
                    <input type="email" className="form-control" id="exampleInputEmail1" aria-describedby="emailHelp"/>
                    <small id="emailHelp" className="form-text text-muted">I'll never share your email with anyone else.</small>
                </div>
                <div className="form-group">
                <label for="exampleInputEmail1">Your Name</label>
                <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp"/>
                </div>
                <div className="form-group">
                <label for="formControlRange">Rate my website</label>
                <input type="range" class="form-control-range" id="formControlRange"/>
                </div>
                <button type="submit" class="btn btn-dark">Submit</button>
            </form>
            <hr/>
            <h3 class="text-center text-muted">Find me on :</h3>
            <div className="container text-center">
                <img src="https://assets.stickpng.com/images/580b57fcd9996e24bc43c521.png" style={{width: "40px"}}/>
                <img src="https://www.freepnglogos.com/uploads/linkedin-blue-style-logo-png-0.png" style={{width: "40px"}}/>
                <img src="https://logodownload.org/wp-content/uploads/2014/09/twitter-logo-2-1.png" style={{width: "40px"}}/>
            </div>

        </div>
        <hr/>
    </div>
  );
}

export default Contact;

