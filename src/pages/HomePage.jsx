import React from 'react'

const Home = () => {
  return (
    <div className="App">
        <br/>
        <h1 className="text-center text-dark">What should you know about me?</h1>
        <hr/>
        <div className="container">
          <div className="row">
            <div className="col-md">
              <div className="card text-white bg-dark">
                <img src="https://images.pexels.com/photos/160107/pexels-photo-160107.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940" className="card-img-top" alt="..." />
                <div className="card-body">
                  <p className="card-text text-center">I am a Front-end web developer Student at Glints Academy Batch 9. Currently learning react to make something awesome.</p>
                </div>
              </div>
            </div>
            <div className="col-md">
              <div className="card text-white bg-dark">
                <img src="https://images.pexels.com/photos/144429/pexels-photo-144429.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940" className="card-img-top" alt="..." />
                <div className="card-body">
                  <p className="card-text text-center">Music has become a part of my life. I can't live a day without singing a song, except when I got canker sore.</p>
                </div>
              </div>
            </div>
            <div className="col-md">
              <div className="card text-white bg-dark">
                <img src="https://images.pexels.com/photos/3771106/pexels-photo-3771106.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940" className="card-img-top" alt="..." />
                <div className="card-body">
                  <p className="card-text text-center">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                </div>
              </div>
            </div>
          </div>
        </div>
        <hr/>
        <div className="container">
          <p class="text-center">Hi, everyone! This is just my imperfect website that I made using React JS and bootsrap. Here I also learn how to use React Router DOM to route my website pages. Don't be surprised that actually this website only have one HTML document (which is index.html). Magic happened, no, this is just React works. It can manipulate that one page using Javascript, so the contents will be changed when we choose it. The main reason why applications built with React are called Single Page Application. So everyone, isn't it cool tho? - React by Facebook</p>
        </div>
        <hr/>
    </div>
  );
}

export default Home;
