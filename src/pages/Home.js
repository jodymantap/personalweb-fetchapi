import React, { Component } from "react";
import './style.css';
 
class Home extends Component {
   constructor(props){
       super(props)
       this.state = {
        items: []
       }
   }

   componentDidMount(){
       fetch("https://api.github.com/users/jodymantap/repos")
        .then(res => res.json())
        //.then(res => console.log(res))
        .then(parsedJSON => parsedJSON.map(data => (
            {
                id: `${data.owner.id}`,
                firstName: `${data.owner.id}`,
                lastName: `${data.owner.login}`,
                location : `${data.owner.html_url}`,
                thumbnail: `${data.owner.avatar_url}`,
            }
        )))
        .then(items => this.setState({
            items
        }))
        .catch(error => console.log('parsing data is failed', error))
   }

   render(){
       const{items} = this.state
       return(
           <div className="boxWhite">
               <h2>Random User</h2>
               {
                   items.length > 0 ? items.map(item => {
                       const {id, firstName, lastName, location, thumbnail} = item
                       return(
                           <div key={id} className="bgCircle">
                               <center><img style={{width: "120px"}} src={thumbnail} alt={firstName} className="circle"></img></center>
                               <div className="info">
                                   {firstName} {lastName} <br/>
                                   {location}
                               </div>
                           </div>
                       )
                   }) 
                :null}
           </div>
       )
   }
}

export default Home;