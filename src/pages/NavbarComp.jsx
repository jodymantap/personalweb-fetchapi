import React from 'react'
import {Link} from 'react-router-dom'

const NavbarComp = () => {
  return (
    <div className="">
        <nav className="navbar navbar-expand-lg navbar-dark bg-dark">
          <a className="navbar-brand" href="/"><span className="text-info">My</span>Profile</a>
          <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
            <span className="navbar-toggler-icon"></span>
          </button>
          <div className="collapse navbar-collapse" id="navbarNavAltMarkup">
            <div className="navbar-nav">
              <Link to="/">
              <a className="nav-link active">Home <span class="sr-only">(current)</span></a>
              </Link>
              <Link to="/profile">
              <a className="nav-link">Profile</a>
              </Link>
              <Link to="/articles">
              <a className="nav-link">Articles</a>
              </Link>
              <Link to="/contact">
              <a className="nav-link">Contact</a>
              </Link>
              <div className="nav-link">
                  <div className="dropdown">
                    <a className="dropdown-toggle text-light" data-toggle="dropdown" href="#">Random Users</a>
                    <ul className="dropdown-menu" role="menu" aria-labelledby="dLabel">
                      <Link to="/home">            
                      <li className="nav-link"><a className="text-secondary" href="#">All Nations</a></li>
                      </Link>
                      <Link to="/swiss">
                      <li className="nav-link"><a className="text-secondary" href="#">Swiss</a></li>
                      </Link>
                      <Link to="/turkey">
                      <li className="nav-link"><a className="text-secondary" href="#">Turkey</a></li>
                      </Link>
                      <Link to="/germany">
                      <li className="nav-link"><a className="text-secondary" href="#">Germany</a></li>
                      </Link>
                    </ul>
                  </div>
              </div>
            </div>
          </div>
        </nav>
    </div>
  );
}

export default NavbarComp;
