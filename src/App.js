import React from 'react';
//import { BrowserRouter } from 'react-router-dom';
import HomePage from './pages/HomePage';
import ProfilePage from './pages/ProfilePage';
import Articles from './pages/Articles';
import Contact from './pages/Contact';
import {BrowserRouter, Route, Switch} from "react-router-dom";
import NavbarComp from './pages/NavbarComp';
import Home from './pages/Home';
import Swiss from './pages/Swiss';
import Turkey from './pages/Turkey';
import Germany from './pages/Germany';
import Belajar from './pages/Belajar';
import HomeAxios from './pages/HomeAxios';
function App() {
  return (
    <BrowserRouter>
      <NavbarComp />
        <Switch>
          <Route exact path="/" component={HomePage}/>
          <Route exact path="/profile" component={ProfilePage}/>
          <Route exact path="/articles" component={Articles}/>
          <Route exact path="/contact" component={Contact}/>
          <Route exact path="/home" component={Home}/>
          <Route exact path="/swiss" component={Swiss}/>
          <Route exact path="/turkey" component={Turkey}/>
          <Route exact path="/germany" component={Germany}/>
          <Route exact path="/belajar" component={Belajar}/>
          <Route exact path="/homeaxios" component={HomeAxios}/>
        </Switch>
    </BrowserRouter>
    
      
  );
}

export default App;
